#!/bin/bash

chombo_ext="chombo.hdf5"
log_ext="log"


chombo_basename()
{
  minres=$1
  maxres=$2
  npref=$3

  echo "${snapshot_abrv}_${minres}_${maxres}_${npref}"
}


chombo_file()
{
  minres=$1
  maxres=$2
  npref=$3

  base_name=$( chombo_basename ${minres} ${maxres} ${npref} )

  echo "${base_name}.${chombo_ext}"
}

chombo_logfile()
{
  minres=$1
  maxres=$2
  npref=$3

  base_name=$( chombo_basename ${minres} ${maxres} ${npref} )

  echo "${base_name}.${log_ext}"
}

radamesh_version_id()
{
  minres=$1
  maxres=$2
  npref=$3
  gamma_bg=$4
  heatr_bg=$5
  ssthr=$6
  case_A=$7

  gamma_tag="$(echo ${gamma_bg} | sed 's/ /_/g')"
  heatr_tag="$(echo ${heatr_bg} | sed 's/ /_/g')"

  base_name=$( chombo_basename ${minres} ${maxres} ${npref} )

  if [ "$case_A" = true ]; then
    echo "${base_name}_${gamma_tag}_${heatr_tag}_${ssthr}_CaseA"
  else
    echo "${base_name}_${gamma_tag}_${heatr_tag}_${ssthr}_CaseB"
  fi
}

init_evol_file()
{
  minres=$1
  maxres=$2
  npref=$3
  gamma_bg=$4
  heatr_bg=$5
  ssthr=$6
  case_A=&7

  version_id="$( radamesh_version_id ${minres} ${maxres} ${npref} "${gamma_bg}" "${heatr_bg}" ${ssthr} ${case_A} )"

  echo "SO.${version_id}.ts0000"
}

init_evol_log()
{
  minres=$1
  maxres=$2
  npref=$3
  gamma_bg=$4
  heatr_bg=$5
  ssthr=$6
  case_A=$7

  version_id="$( radamesh_version_id ${minres} ${maxres} ${npref} "${gamma_bg}" "${heatr_bg}" ${ssthr} ${case_A} )"

  echo "SO.${version_id}.${log_ext}"
}

column_density_basename()
{
  input=$(basename $1)
  var=$2
  proj=$3
  lmax=$4

  echo "${input}_var_${var}_proj_${proj}_lmax_${lmax}"
}

column_density_file()
{
  input=$(basename $1)
  var=$2
  proj=$3
  lmax=$4

  base_name=$( column_density_basename ${input} ${var} ${proj} ${lmax} )

  echo "${base_name}.fits"
}

column_density_logfile()
{
  input=$(basename $1)
  var=$2
  proj=$3
  lmax=$4

  base_name=$( column_density_basename ${input} ${var} ${proj} ${lmax} )

  echo "${base_name}.${log_ext}"
}

fits_3d_basename()
{
  input=$(basename $1)
  var=$2
  lmax=$3

  base_name=$( chombo_basename ${minres} ${maxres} ${npref} )

  echo "${base_name}_var_${var}_lmax_${lmax}"
}

fits_3d_file()
{
  input=$(basename $1)
  var=$2
  lmax=$3

  base_name=$( fits_3d_basename ${input} ${var} ${lmax} )

  echo "${base_name}.fits"
}

fits_3d_logfile()
{
  input=$(basename $1)
  var=$2
  lmax=$3

  base_name=$( fits_3d_basename ${input} ${var} ${lmax} )

  echo "${base_name}.${log_ext}"
}
