#!/bin/bash
#
# Usage
# ./chombo.sh <min-resolution> <max-resolution> <npref>
# ./chombo.sh 512 4096 8


source ../set_me.sh
source ../filename.sh


minres=$1
maxres=$2
npref=$3

exe=./P2C/build/P2C_serial

chombo_file ${minres} ${maxres} ${npref}

output=$(chombo_file ${minres} ${maxres} ${npref})
logfile=$(chombo_logfile ${minres} ${maxres} ${npref})

lmax=$( printf "%.0f" $(echo "(l(${maxres}) / l(2)) - (l(${minres}) / l(2))" | bc -l) )


echo "${exe} -inp ${eagle_snapshot} -out ${output} -base_grid ${minres} -lmax ${lmax} -npref ${npref} -file_fmt eaglefmt" | tee -a ${logfile}
${exe} -inp ${eagle_snapshot} -out ${output} -base_grid ${minres} -lmax ${lmax} -npref ${npref} -file_fmt eaglefmt | tee -a ${logfile}
