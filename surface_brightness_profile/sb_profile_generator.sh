#!/bin/bash
#
# Usage
# ./sb_profile_generator.sh <n_threads> <SF_number_density_thr>

source ../set_me.sh

python3 ./sb_profile_generator.py ${eagle_snapshot} $1 $2
