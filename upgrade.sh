#!/bin/bash
#
# USAGE: ./upgrade.sh

git pull origin master
git submodule update --init --recursive

# Building P2C
cd ./chombo/P2C
rm -rf build && mkdir -p build
cd build
cmake .. && make
cd ../../..

# Building column_density/PlotStArt
make clean -C ./column_density/PlotStArt
make -C ./column_density/PlotStArt

# Building fits_3d/PlotStArt
make clean -C ./fits_3d/PlotStArt/
make -C ./fits_3d/PlotStArt/

# Building Radamesh
mkdir -p ./init_evol/Radamesh/bin
make clean -C ./init_evol/Radamesh
make -C ./init_evol/Radamesh

