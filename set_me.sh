#!/bin/bash
#
# Make sure to edit this file before running other scripts
# eagle_snapshot="../../../../EAGLE_snapshots/RefL0025N0752/snapshot_010_z003p984/snap_010_z003p984.%d.hdf5"
# snapshot_abrv="snap_010_z003p984"

args=(%args%)

eagle_snapshot="${args[0]}"
snapshot_abrv="${args[1]}"
