#!/bin/bash
#
# Usage
# ./column_density_layers.sh <input> <variable> <lmax> <# layers> <# cores>
# ./column_density_layers.sh /path/to/input/chombo NHI 2 29 16


source ../set_me.sh
source ../filename.sh


input="$1"
var=$2
lmax=$3
nlayers=$4
ncores=$5

exe=./PlotStArt/bin/PlotStART-0.1.0.a


basename_x=$(column_density_basename ${input} ${var} 1 ${lmax})
#basename_y=$(column_density_basename ${input} ${var} 2 ${lmax})
#basename_z=$(column_density_basename ${input} ${var} 3 ${lmax})


chunk=$( echo "${ncores} / 3" | bc )
nchunks=$( echo "${nlayers}/${chunk} + 1" | bc )

width=$( echo "scale=7; 1/${nlayers}" | bc )

for c in $(seq ${nchunks} ); do

  minl=$( echo "scale=0; ${chunk} * ($c - 1) + 1" | bc )
  maxl=$( echo "scale=0; ${chunk} * ($c - 1) + ${chunk}" | bc )
  [[ ${maxl} -gt ${nlayers} ]] && maxl=${nlayers} || ture

  pids=()

  for layer in $( seq ${minl} ${maxl} ); do

    l=$( echo "scale=7; (${layer} - 1) * ${width}" | bc )
    r=$( echo "scale=7; ${l} + ${width}" | bc )

    output_x="layers/${basename_x}_l_${layer}_${nlayers}.fits" 
    #output_y="layers/${basename_y}_l_${layer}_${nlayers}.fits"
    #output_z="layers/${basename_z}_l_${layer}_${nlayers}.fits"

    log_x="layers/${basename_x}_l_${layer}_${nlayers}.log" 
    #log_y="layers/${basename_y}_l_${layer}_${nlayers}.log"
    #log_z="layers/${basename_z}_l_${layer}_${nlayers}.log"

    echo "${exe} -inp ${input} -out ${output_x} -var ${var} -proj 1 -box_le ${l},0,0 -box_re ${r},1,1 -ftype fits" | tee -a ${log_x}
    #echo "${exe} -inp ${input} -out ${output_y} -var ${var} -proj 2 -box_le 0,${l},0 -box_re 1,${r},1 -ftype fits" | tee -a ${log_y}
    #echo "${exe} -inp ${input} -out ${output_z} -var ${var} -proj 3 -box_le 0,0,${l} -box_re 1,1,${r} -ftype fits" | tee -a ${log_z}

    ${exe} -inp ${input} -out ${output_x} -var ${var} -proj 1 -box_le "${l},0,0" -box_re "${r},1,1" -ftype fits > ${log_x} &
    pids+=($!)
    #${exe} -inp ${input} -out ${output_y} -var ${var} -proj 2 -box_le "0,${l},0" -box_re "1,${r},1" -ftype fits > ${log_y} &
    #pids+=($!)
    #${exe} -inp ${input} -out ${output_z} -var ${var} -proj 3 -box_le "0,0,${l}" -box_re "1,1,${r}" -ftype fits > ${log_z} &
    #pids+=($!)
  done

  for pid in ${pids}; do
    wait ${pid}
  done
done


