#!/bin/bash
#
# Usage
# ./column_density.sh <input> <var> <proj> <lmax>
# ./column_density.sh /path/to/the/input/file NHI 1 2


source ../set_me.sh
source ../filename.sh


input="$1"
var=$2
proj=$3
lmax=$4

exe=./PlotStArt/bin/PlotStART-0.1.0.a

output=$(column_density_file ${input} ${var} ${proj} ${lmax})
logfile=$(column_density_logfile ${input} ${var} ${proj} ${lmax})


echo "${exe} -inp ${input} -out ${output} -var ${var} -proj ${proj} -lmax ${lmax} -ftype fits" | tee -a ${logfile}
${exe} -inp ../init_evol/${input} -out ${output} -var ${var} -proj ${proj} -lmax ${lmax} -ftype fits | tee -a ${logfile}
